import {Paciente} from './paciente';

export class Signo {
  public idSigno: number;
  public paciente: Paciente;
  public fecha: string;
  public temperatura: number;
  public pulso: number;
  public ritmoRespiratorio: number;
}
