import { Component, OnInit } from '@angular/core';
import {Menu} from '../../../_model/menu';
import {Rol} from '../../../_model/rol';
import {MenuService} from '../../../_service/menu.service';
import {RolService} from '../../../_service/rol.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';

@Component({
  selector: 'app-menu-rol-edicion',
  templateUrl: './menu-rol-edicion.component.html',
  styleUrls: ['./menu-rol-edicion.component.css']
})
export class MenuRolEdicionComponent implements OnInit {

  id: number;
  menu: Menu;
  edicion: boolean = false;

  idMenuBypass: number;
  nombreBypass: string;
  iconoBypass: string;
  urlBypass: string;
  rolesBypass: Rol[];
  roles: Rol[];

  idRolSeleccionado: number;
  rolesSeleccionados: Rol[] = [];
  mensaje: string;

  constructor(private menuService: MenuService, private rolService: RolService, private route: ActivatedRoute, private router: Router,
              public snackBar: MatSnackBar) {
    this.menu = new Menu();
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
      this.limpiarControles();
    });

    this.listarRoles();
  }

  private initForm() {
    if (this.edicion) {
      this.menuService.listarMenuPorId(this.id).subscribe(data => {
        this.idMenuBypass = data.idMenu;
        this.nombreBypass = data.nombre;
        this.iconoBypass = data.icono;
        this.urlBypass = data.url;
        this.rolesBypass = data.roles;
      });
    }
  }

  operar() {
    let menu = new Menu();
    menu.idMenu = this.idMenuBypass;
    menu.nombre = this.nombreBypass;
    menu.icono = this.iconoBypass;
    menu.url = this.urlBypass;

    let rolesTotales = this.rolesBypass.concat(this.rolesSeleccionados);
    menu.roles = rolesTotales;
    //menu.roles = this.rolesSeleccionados;

    this.menuService.modificar(menu).subscribe(data => {
      this.menuService.listarMenus().subscribe(roles => {
        this.menuService.menuCambio.next(roles);
        this.menuService.mensaje.next('Se actualizó');
        this.router.navigate(['menu-rol']);
      });
    });

  }// Operar

  listarRoles() {
    this.rolService.listarRoles().subscribe(data => {
      this.roles = data;
    });
  }

  agregarRol() {
    if (this.idRolSeleccionado > 0) {
      let cont = 0; let cont2 = 0;
      //Evitar roles duplicados: Comparar con lista de ROLES nuevos recién asignados
      for (let i = 0; i < this.rolesSeleccionados.length; i++) {
        let rol = this.rolesSeleccionados[i];
        if (rol.idRol === this.idRolSeleccionado) {
          cont++;
          break;
        }
      }
      //Evitar roles duplicados: Comparar con lista de ROLES ya existentes
      for (let i = 0; i < this.rolesBypass.length; i++) {
        let rol = this.rolesBypass[i];
        if (rol.idRol === this.idRolSeleccionado) {
          cont2++;
          break;
        }
      }
      if (cont > 0) {
        this.mensaje = `Rol duplicado: El rol se acaba de asignar.`;
        this.snackBar.open(this.mensaje, "Aviso", { duration: 3000 });
      } else
      if (cont2 > 0) {
        this.mensaje = `Rol duplicado: El rol ya se encuentra asignado.`;
        this.snackBar.open(this.mensaje, "Aviso", { duration: 3000 });
      } else {
        let rol_ = new Rol();
        this.rolService.listarRolPorId(this.idRolSeleccionado).subscribe(data => {
          //Chasky:
          rol_ = data;
          this.rolesSeleccionados.push(rol_);
          // rol_.idRol = this.idRolSeleccionado;
          // this.rolService.listarRolPorId(this.idRolSeleccionado).subscribe(data => {
          //   rol_.nombre = data.nombre;
          //   this.rolesSeleccionados.push(rol_);
        });
      }
    } else {
      this.mensaje = `Debe agregar un rol`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }
  }

  removerRol(index: number) {
    this.rolesSeleccionados.splice(index, 1);
  }

  removerRolBypass(index: number) {
    this.rolesBypass.splice(index, 1);
  }

  limpiarControles() {
    this.rolesSeleccionados = [];
    this.idRolSeleccionado = 0;
    this.rolesBypass = [];
  }

}
