import {Component, OnInit, ViewChild} from '@angular/core';
import {Menu} from '../../_model/menu';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {MenuService} from '../../_service/menu.service';

@Component({
  selector: 'app-menu-rol',
  templateUrl: './menu-rol.component.html',
  styleUrls: ['./menu-rol.component.css']
})
export class MenuRolComponent implements OnInit {

  lista: Menu[] = [];
  displayedColumns = ['idMenu', 'nombre', 'icono', 'url', 'acciones'];
  dataSource: MatTableDataSource<Menu>;
  cantidad: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private menuService: MenuService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.menuService.menuCambio.subscribe(data => {
      this.lista = data;
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this.menuService.mensaje.subscribe(data2 => {
        this.snackBar.open(data2, 'Aviso', { duration: 2000 });
      });
    });

    this.menuService.listarMenusPageable(0, 10).subscribe(data => {
      let menus = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(menus);
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(idRol: number) {
    this.menuService.eliminar(idRol).subscribe(data => {
        this.menuService.listarMenus().subscribe(data2 => {
          this.lista = data2;
          this.dataSource = new MatTableDataSource(this.lista);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        });
      }
    );
  }

  mostrarMas(e: any) {
    console.log(e);
    this.menuService.listarMenusPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let menus = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource = new MatTableDataSource(menus);
      this.dataSource.sort = this.sort;
    });
  }
}
